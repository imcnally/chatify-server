app = require('express')()
server = require('http').createServer app
io = require('socket.io').listen server
port = process.env.PORT or 5000

server.listen port, ->
    console.log "Listening on #{port}\n"

# heroku socket config
io.configure ->
  io.set 'transports', ['xhr-polling']
  io.set 'polling duration', 10

io.sockets.on 'connection', (socket) ->

    socket.on 'messageSent', (message) ->
      io.sockets.emit 'newMessage', message

    socket.on 'trackUpdate', (listener) ->
      io.sockets.emit 'listenerUpdate', listener
